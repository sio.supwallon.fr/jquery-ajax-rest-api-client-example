$(document).ready(function(){
    $("#get").click(function(){
        alert("Get");
        $.ajax({
            url: 'https://localhost:44394/default/',
            type: 'GET',
            success: function(data, textStatus, jqXHR) {
                // Multiple values received
                data.forEach(element => {
                    $('#result').append(element + '\r\n')
                });
            },
            error: function(result, status, error){
                $('#result').html(error);
            }
        });
    });
    $("#post").click(function(){
        alert("Post");

        $.ajax({
            url: 'https://localhost:44394/default/',
            type: 'POST',
            data: JSON.stringify({'id': '1'}),
            contentType: 'application/json',
            success: function(data, textStatus, jqXHR) {
                $('#result').html('POST OK')
            }
        });
    });
    $("#put").click(function(){
        alert("Put");

        $.ajax({
            url: 'https://localhost:44394/default/1',
            type: 'PUT',
            data: JSON.stringify({'id': '1', 'value': 'test'}),
            contentType: 'application/json',
            success: function(data, textStatus, jqXHR) {
                $('#result').html('PUT OK')
            }
        });
    });
    $("#delete").click(function(){
        alert("Delete");

        $.ajax({
            url: 'https://localhost:44394/default/1',
            type: 'DELETE',
            success: function(data, textStatus, jqXHR) {
                $('#result').html('DELETE OK')
            }
        });
    });
});